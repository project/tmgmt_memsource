TMGMT Phrase TMS (tmgmt_memsource)
---------------------

TMGMT Phrase TMS module is a plugin for
Translation Management Tool module (tmgmt).
It uses the Phrase TMS (https://www.phrase.com)
for automated translation of the content.

REQUIREMENTS
------------

This module requires TMGMT (http://drupal.org/project/tmgmt) module
to be installed.

Also, you will need to enter your Phrase TMS credentials.
You can obtain them by registering on https://www.phrase.com.

RELEASE NOTES
-------------

https://support.phrase.com/hc/en-us/articles/5743706903964-Plugin-Releases
